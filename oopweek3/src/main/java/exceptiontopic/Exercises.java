package exceptiontopic;

public class Exercises {
    /*
Exercise01

Is the following code legal?
Search, read, learn and write a detailed answer

    try {
    ...
    } finally {
    ...
    }

Answer: Yes, it's legal.
Moreover if there is no any catch block, then a finally block is mandatory.

------------------------
Exercise02

What exception types can be caught by the following handler?
What is wrong with using this type of exception handler?
Write a detailed answer

    catch (Exception e) {
    ...
    }

Answer: It can be either an unchecked or checked exception.
To works this handler properly we need to create a try block before,
where we have to define instruction(s) that will be examine to be able to execute.
If it cannot be execute, an exception object will be created and in the catch block
we can define what should our program do in that specified case.

----------------------------
Exercise03

What exceptions can be caught by the following handler?
Is there anything wrong with this exception handler as written? Will this code compile?
Write a detailed answer

     ...
     } catch (Exception e) {
     ...
     } catch (ArithmeticException a) {
     ...
     }

 Answer: Any types of exception will be caught because Exception is the superclass of all kinds of
 other exceptions. That's why this code won't be compile.

--------------------------------------
 Exercise04

Match a situation (marked by letters a-d) with an item (marked by number 1-4)

     a,    int[] A;
            A[0] = 0;
     b,  The Java VM starts running your program, but the VM can’t find the Java platform classes.
         (The Java platform classes reside in classes.zip or rt.jar.)
     c,  A program is reading a stream and reaches the end of stream marker.
     d,  Before closing the stream and after reaching the end of stream marker, a program tries to read the stream again.
     1,  error
     2,  checked exception
     3,  runtime exception
     4,  no exception

     Answer: a - 1 ; b - 3 ; c - 4 ; d -2

------------------------------------------------
Exercise05

Modify the following code block so that it will compile

public static void cat(File named) {
    RandomAccessFile input = null;
    String line = null;

    try {
        input = new RandomAccessFile(named, "r");
        while ((line = input.readLine()) != null) {
            System.out.println(line);
        }
        return;
    } finally {
        if (input != null) {
            input.close();
        }
    }
}

Answer:

public static void cat(File named) {
    RandomAccessFile input = null;
    String line = null;

    while ((line = input.readLine()) != null) {
    try {
        input = new RandomAccessFile(named, "r");
            System.out.println(line);
        }
        return;
    finally {
        if (input != null) {
            input.close();
        }
    }
}

------------------------------------------





     */
}
