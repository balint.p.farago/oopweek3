package frigoexercise;

import java.util.ArrayList;
import java.util.Iterator;

public class PracticeIterator {

    public static void main(String[] args) {


        ArrayList<Integer> intList = new ArrayList<>();

        intList.add(7);
        intList.add(3);
        intList.add(2);
        intList.add(1);
        intList.add(9);
        intList.add(5);
        intList.add(11);

        /*for (int i = 0; i < intList.size(); i++){
            System.out.println(intList.get(i));
        }*/

        intList.forEach(System.out::println);

        /*for (int i = 0; i < intList.size(); i++){
            intList.remove(i);
        }
*/

        Iterator<Integer> itr = intList.iterator();

         // it will cause an Illegal state exception
        /*while (itr.hasNext()){
            itr.remove();
        }
*/

        while (itr.hasNext()){
            int num = itr.next().intValue();
            if (num % 2 == 0) {
                itr.remove();
            }
        }


        for (int i = 0; i < intList.size(); i++){
            System.out.println(intList.get(i));
        }

    }
}
