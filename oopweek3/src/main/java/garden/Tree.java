package garden;

public class Tree extends Plant{

    public Tree(String name) {
        super.name = name;
    }

    @Override
    boolean isNeedWater() {
        boolean isNeedWater = false;
        if (waterLevel < 10){
            isNeedWater = true;
        }
        return isNeedWater;
    }

    @Override
    String printWaterState() {
        String printWaterState;
        if (isNeedWater() == true){
            printWaterState = " needs water.";
        }else{
            printWaterState = " does not need water.";
        }
        return printWaterState;
    }

    @Override
    double water(double water) {
        double absorbedWater = water * 0.4;
        return absorbedWater;
    }
}
