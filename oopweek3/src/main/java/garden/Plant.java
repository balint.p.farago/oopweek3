package garden;

public abstract class Plant {

    double waterLevel;
    String name;

    abstract double water(double water);

    abstract boolean isNeedWater();

    abstract String printWaterState();
}
