package garden;

import java.util.ArrayList;

public class Garden {

    ArrayList<Plant> garden = new ArrayList<Plant>();

    void addWater(int amountOfWater){
        double numberOfPlantsNeedWater = 0;
        double sortedWater;
        for (Plant aGarden : garden) {
            if (aGarden.isNeedWater() == true) {
                numberOfPlantsNeedWater++;
            }
        }
        sortedWater = amountOfWater / numberOfPlantsNeedWater;
        for (int i = 0; i < garden.size(); i++){
            if (garden.get(i).isNeedWater() == true){
                garden.get(i).waterLevel = garden.get(i).waterLevel + garden.get(i).water(sortedWater);
            }
        }
        System.out.println("\n" + "After watering with: " + amountOfWater + ":" + "\n");
        printGarden();
    }

    void addPlant(Plant plant){
        garden.add(plant);
    }

/*
    void removePlant(Plant plant){
        garden.remove(plant);
    }
*/

    void printGarden(){

        for (int i = 0; i < garden.size(); i++){
            System.out.println("The " + garden.get(i).name + garden.get(i).printWaterState());
        }
    }
}
