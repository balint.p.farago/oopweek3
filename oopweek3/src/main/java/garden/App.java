package garden;

public class App {

    public static void main(String[] args) {

        Garden myGarden = new Garden();

        Plant plant1 = new Flower("Yellow flower");
        myGarden.addPlant(plant1);
        Plant plant2 = new Flower("Blue flower");
        myGarden.addPlant(plant2);
        Plant plant3 = new Tree("Purple tree");
        myGarden.addPlant(plant3);
        Plant plant4 = new Tree("Purple tree");
        myGarden.addPlant(plant4);

        myGarden.printGarden();

        myGarden.addWater(40);

        myGarden.addWater(100);
    }
}
