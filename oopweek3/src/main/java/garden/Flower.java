package garden;

public class Flower extends Plant {

    public Flower(String name) {
        super.name = name;
    }

    @Override
    boolean isNeedWater() {
        boolean isNeedWater = false;
        if (waterLevel < 5){
            isNeedWater = true;
        }
        return isNeedWater;
    }

    @Override
    String printWaterState() {
        String printWaterState;
        if (isNeedWater() == true){
            printWaterState = " needs water.";
        }else{
            printWaterState = " does not need water.";
        }
        return printWaterState;
    }

    @Override
    double water(double water) {
        double absorbedWater = water * 0.75;
        return absorbedWater;
    }
}
