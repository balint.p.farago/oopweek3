import java.util.Scanner;

public class TestWhile {

    public static void main(String[] args) {
        play();
    }

    static boolean isAnInteger(String input){
        boolean isAnInteger = false;
        try{
            Integer.parseInt(input);
            isAnInteger = true;
        }catch (NumberFormatException ex) {

        }
        return isAnInteger;
    }

    static void play () {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter a slot number to take an \"X\" to it. ");
        String stringXInput = sc.nextLine();
        while (!isAnInteger(stringXInput)) {
            System.out.println("This is not a number. Please try it again.");
            stringXInput = sc.nextLine();
        }
        int xInput;
        xInput = Integer.parseInt(stringXInput);
        System.out.println(" the input was correct: " + xInput);
    }
}
