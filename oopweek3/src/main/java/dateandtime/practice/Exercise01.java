package dateandtime.practice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Exercise01 {

    public static void main(String[] args) {

        Calendar myCalendar = new Calendar() {
            @Override
            protected void computeTime() {

            }

            @Override
            protected void computeFields() {

            }

            @Override
            public void add(int field, int amount) {

            }

            @Override
            public void roll(int field, boolean up) {

            }

            @Override
            public int getMinimum(int field) {
                return 0;
            }

            @Override
            public int getMaximum(int field) {
                return 0;
            }

            @Override
            public int getGreatestMinimum(int field) {
                return 0;
            }

            @Override
            public int getLeastMaximum(int field) {
                return 0;
            }
        };

        Calendar cal = new GregorianCalendar();


/*
        System.out.println(myCalendar.getTime());
        System.out.println(myCalendar.getActualMaximum(Calendar.YEAR));
*/
        System.out.println(cal.get(Calendar.SECOND) + " "
                + cal.get(Calendar.MINUTE) + " "
                + cal.get(Calendar.HOUR) + " "
                + cal.get(Calendar.MONTH) + " "
                + cal.get(Calendar.DATE) + " "
                + cal.get(Calendar.YEAR));

        System.out.println(cal.getTime());
        System.out.println(myCalendar.getTime());
        System.out.println();
        System.out.println("maximum year: " + cal.getActualMaximum(Calendar.YEAR) + "\n"
                + "maximum month: " + cal.getActualMaximum(Calendar.MONTH) + "\n"
                + "maximum week of year: " + cal.getActualMaximum(Calendar.WEEK_OF_YEAR) + "\n"
                + "maximum week of month: " + cal.getActualMaximum(Calendar.WEEK_OF_MONTH) + "\n"
                + "maximum day of the current month: " + cal.getActualMaximum(Calendar.DATE) + "\n");

        System.out.println("minimum year: " + cal.getActualMinimum(Calendar.YEAR) + "\n"
                + "minimum month: " + cal.getActualMinimum(Calendar.MONTH) + "\n"
                + "minimum week of year: " + cal.getActualMinimum(Calendar.WEEK_OF_YEAR) + "\n"
                + "minimum week of month: " + cal.getActualMinimum(Calendar.WEEK_OF_MONTH) + "\n"
                + "minimum day of the current month: " + cal.getActualMinimum(Calendar.DATE));
        System.out.println();
        TimeZone newYorkTimeZone = TimeZone.getTimeZone("EST");
        SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        myDateFormat.setTimeZone(newYorkTimeZone);
        cal.setTimeZone(newYorkTimeZone);
        System.out.println(myDateFormat.format(cal.getTime()));

    }
}
