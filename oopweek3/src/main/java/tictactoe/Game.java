package tictactoe;

import java.util.Scanner;

public class Game {

    public static void main(String[] args) {

        String[][] table = new String[3][3];
        fillTable(table);
        printTable(table);
        playGame(table);
    }

    static void fillTable(String[][] table) {
        int slot = 0;
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                slot++;
                table[row][col] = Integer.toString(slot);

            }
        }
    }

    static String border() {
        return "/---|---|---\\";
    }

    static String innerBoarder() {
        return "|-----------|";
    }

    static void printTable(String[][] table) {
        int i;
        System.out.println();
        System.out.println(border());
        for (i = 0; i < 3; i++) {
            System.out.print("|" + " " + table[0][i] + " ");
        }
        System.out.print("|");
        System.out.println("\n" + innerBoarder());
        for (i = 0; i < 3; i++) {
            System.out.print("|" + " " + table[1][i] + " ");
        }
        System.out.print("|");
        System.out.println("\n" + innerBoarder());
        for (i = 0; i < 3; i++) {
            System.out.print("|" + " " + table[2][i] + " ");
        }
        System.out.print("|");
        System.out.println("\n" + border());
        System.out.println();
    }

    static int inputToRow(int input){
        int row = 0;
        if (input == 1 || input == 2 || input == 3){
            row = 0;
        }else  if (input == 4 || input == 5 || input == 6){
            row = 1;
        }else if (input == 7 || input == 8 || input == 9){
            row = 2;
        }
        return row;
    }

    static int inputToColumn(int input){
        int col = 0;
        if (input == 1 || input == 4 || input == 7){
            col = 0;
        }else  if (input == 2 || input == 5 || input == 8){
            col = 1;
        }else if (input == 3 || input == 6 || input == 9){
            col = 2;
        }
        return col;
    }

    static boolean isXWinner(String [][] table){
        boolean xWin = false;

        //checking the rows
        if (table[0][0].equals("X") && table[0][1].equals("X") && table[0][2].equals("X")){
            xWin = true;
        }else if (table[1][0].equals("X") && table[1][1].equals("X") && table[1][2].equals("X")){
            xWin = true;
        }else if (table[2][0].equals("X") && table[2][1].equals("X") && table[2][2].equals("X")){
            xWin = true;
            //checking the columns
        }else if (table[0][0].equals("X") && table[1][0].equals("X") && table[2][0].equals("X")){
            xWin = true;
        }else if (table[0][1].equals("X") && table[1][1].equals("X") && table[2][1].equals("X")) {
            xWin = true;
        }else if (table[0][2].equals("X") && table[1][2].equals("X") && table[2][2].equals("X")) {
            xWin = true;
            //checking diagonal
        }else if (table[0][0].equals("X") && table[1][1].equals("X") && table[2][2].equals("X")) {
            xWin = true;
        }else if (table[2][0].equals("X") && table[1][1].equals("X") && table[0][2].equals("X")) {
            xWin = true;
        }

        return  xWin;
    }

    static boolean isOWinner(String[][] table){
        boolean oWin = false;

        //checking the rows
        if (table[0][0].equals("O") && table[0][1].equals("O") && table[0][2].equals("O")){
            oWin = true;
        }else if (table[1][0].equals("O") && table[1][1].equals("O") && table[1][2].equals("O")){
            oWin = true;
        }else if (table[2][0].equals("O") && table[2][1].equals("O") && table[2][2].equals("O")){
            oWin = true;
            //checking the columns
        }else if (table[0][0].equals("O") && table[1][0].equals("O") && table[2][0].equals("O")){
            oWin = true;
        }else if (table[0][1].equals("O") && table[1][1].equals("O") && table[2][1].equals("O")) {
            oWin = true;
        }else if (table[0][2].equals("O") && table[1][2].equals("O") && table[2][2].equals("O")) {
            oWin = true;
            //checking diagonal
        }else if (table[0][0].equals("O") && table[1][1].equals("O") && table[2][2].equals("O")) {
            oWin = true;
        }else if (table[2][0].equals("O") && table[1][1].equals("O") && table[0][2].equals("O")) {
            oWin = true;
        }

        return  oWin;
    }

    static boolean isThereSlotRemaining(String[][] table){
        boolean isThereSlotRemaining = true;
        if (!table[0][0].equals("1")
                && !table[0][1].equals("2")
                && !table[0][2].equals("3")
                && !table[1][0].equals("4")
                && !table[1][1].equals("5")
                && !table[1][2].equals("6")
                && !table[2][0].equals("7")
                && !table[2][1].equals("8")
                && !table[2][2].equals("9")){
            isThereSlotRemaining = false;
        }
        return isThereSlotRemaining;
    }

    static boolean isAnInteger(String input){
        boolean isAnInteger = false;
        try{
            Integer.parseInt(input);
            isAnInteger = true;
        }catch (NumberFormatException ex) {

        }
        return isAnInteger;
    }

    static void playGame(String[][] table) {

        int stepCounter = 0;
        if (stepCounter == 0) {
            System.out.println(" Welcome to the 2-player Tic Tac Toe game."
                    + "\n" + "Please choose a number 1 - 9 to take your sign to the slot."
                    + "\n" + "X will be the first." + "\n");
        }

        while (!isXWinner(table) || !isOWinner(table)) {

            Scanner sc = new Scanner(System.in);
            System.out.println("The next turn is player \"X\":");
            System.out.println("Please enter a slot number to take an \"X\" to it. ");
            String stringXInput = sc.nextLine();
            if (isAnInteger(stringXInput)){
                while(isAnInteger(stringXInput)) {
                    int xInput;
                    xInput = Integer.parseInt(stringXInput);
                    int column;
                    int row;
                    column = inputToColumn(xInput);
                    row = inputToRow(xInput);
                    if (table[row][column].equals("X") || table[row][column].equals("O")) {
                        System.out.println("This slot is already taken. Please choose another:");
                        printTable(table);
                        stringXInput = sc.nextLine();
                    }else{
                        break;
                    }
                }
            }
            while (!isAnInteger(stringXInput)) {
                if (!isAnInteger(stringXInput)) {
                    System.out.println("This is not a number. Please try it again.");
                    printTable(table);
                }
                stringXInput = sc.nextLine();
                while(isAnInteger(stringXInput)){
                    int xInput;
                    xInput = Integer.parseInt(stringXInput);
                    int column;
                    int row;
                    column = inputToColumn(xInput);
                    row = inputToRow(xInput);
                    if (table[row][column].equals("X") || table[row][column].equals("O")) {
                        System.out.println("This slot is already taken. Please choose another:");
                        printTable(table);
                        stringXInput = sc.nextLine();
                    }
                    else{
                        break;
                    }
                }
            }
            int oInput;
            oInput = Integer.parseInt(stringXInput);
            int column;
            int row;
            column = inputToColumn(oInput);
            row = inputToRow(oInput);
            table[row][column] = "X";
            if (isXWinner(table)) {
                System.out.println("X is the winner. Congrat!" + "\n");
                printTable(table);
                return;
            } else if (!isThereSlotRemaining(table)) {
                System.out.println("The game ends. The result is: DRAW" + "\n" + "Thanks for playing with Tic Tac Toe! :)");
                return;
            } else {
                printTable(table);
            }

            // "O" player's turn

            System.out.println("The next turn is player \"O\":");
            System.out.println("Please enter a slot number to take an \"O\" to it. ");
            String stringOInput = sc.nextLine();
            if (isAnInteger(stringOInput)){
                while(isAnInteger(stringOInput)) {
                    oInput = Integer.parseInt(stringOInput);
                    column = inputToColumn(oInput);
                    row = inputToRow(oInput);
                    if (table[row][column].equals("X") || table[row][column].equals("O")) {
                        System.out.println("This slot is already taken. Please choose another:");
                        printTable(table);
                        stringOInput = sc.nextLine();
                    }else{
                        break;
                    }
                }
            }
            while (!isAnInteger(stringOInput)) {
                if (!isAnInteger(stringOInput)) {
                    System.out.println("This is not a number. Please try it again.");
                    printTable(table);
                }
                stringOInput = sc.nextLine();
                while(isAnInteger(stringOInput)){
                    oInput = Integer.parseInt(stringOInput);
                    column = inputToColumn(oInput);
                    row = inputToRow(oInput);
                    if (table[row][column].equals("X") || table[row][column].equals("O")) {
                        System.out.println("This slot is already taken. Please choose another:");
                        printTable(table);
                        stringOInput = sc.nextLine();
                    }
                    else{
                        break;
                    }
                }
            }

            oInput = Integer.parseInt(stringOInput);
            column = inputToColumn(oInput);
            row = inputToRow(oInput);
            table[row][column] = "O";
            if (isOWinner(table)) {
                System.out.println("O is the winner. Congrat!" + "\n");
                printTable(table);
                return;
            } else if (!isThereSlotRemaining(table)) {
                System.out.println("The game ends. The result is: DRAW" + "\n" + "Thanks for playing with Tic Tac Toe! :)");
                return;
            } else {
                printTable(table);
            }
        }
    }
}
